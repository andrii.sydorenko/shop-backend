psql -d "host=localhost port=5432 dbname=postgres user=postgres"

CREATE TABLE users (
  id SERIAL PRIMARY KEY,
  first_name VARCHAR(255) NOT NULL,
  last_name VARCHAR(255) NOT NULL,
  is_admin BOOLEAN DEFAULT false,
  email VARCHAR(70) UNIQUE NOT NULL,
  password TEXT NOT NULL,
  registration_date TIMESTAMPTZ DEFAULT NOW()
);

CREATE TABLE categories (
  id SERIAL PRIMARY KEY,
  category_name VARCHAR(70) UNIQUE NOT NULL
);

CREATE TABLE products (
  id SERIAL PRIMARY KEY,
  category_id INT,
  title VARCHAR(255) NOT NULL,
  price FLOAT(4) NOT NULL,
  description TEXT,
  viewed INT NOT NULL DEFAULT 0,
  picture_path TEXT NOT NULL,
  size SMALLINT NOT NULL DEFAULT 38,
  creation_date TIMESTAMPTZ DEFAULT NOW(),
  CONSTRAINT fk_category
        FOREIGN KEY(category_id) 
      REFERENCES categories(id)
      ON DELETE cascade
);

-- Unused table

-- CREATE TABLE viewed_products (
--   id SERIAL PRIMARY KEY,
--   user_email VARCHAR(70),
--   product_id INT,
--   CONSTRAINT fk_user
--       FOREIGN KEY(user_email) 
-- 	  REFERENCES users(email),
--   CONSTRAINT fk_product
--         FOREIGN KEY(product_id) 
--       REFERENCES products(id)
-- );

CREATE TABLE feedbacks (
  id SERIAL PRIMARY KEY,
  user_email VARCHAR(70),
  product_id INT NOT NULL,
  feedback_message TEXT NOT NULL,
  sending_date TIMESTAMPTZ DEFAULT NOW(),
  CONSTRAINT fk_product
      FOREIGN KEY(product_id) 
	  REFERENCES products(id)
    ON DELETE cascade
);

CREATE TABLE recommended_products (
  id SERIAL PRIMARY KEY,
  product_id INT NOT NULL UNIQUE,
  CONSTRAINT fk_product
        FOREIGN KEY(product_id) 
      REFERENCES products(id)
      ON DELETE cascade
);

-- Shopping Cart

CREATE TABLE shopping_carts (
  id SERIAL PRIMARY KEY,
  user_email VARCHAR(70),
  CONSTRAINT fk_user
        FOREIGN KEY(user_email) 
      REFERENCES users(email)
      ON DELETE cascade
);

CREATE TABLE cart_items (
  id SERIAL PRIMARY KEY,
  product_id INT NOT NULL UNIQUE,
  quantity INT DEFAULT 1,
  shopping_cart_id INT NOT NULL,
  CONSTRAINT fk_product
        FOREIGN KEY(product_id) 
      REFERENCES products(id)
      ON DELETE cascade,
  CONSTRAINT fk_shopping_cart
        FOREIGN KEY(shopping_cart_id) 
      REFERENCES shopping_carts(id)
      ON DELETE cascade
);