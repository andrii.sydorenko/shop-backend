import { ShoppingCartService } from './../shopping-cart/shopping-cart.service';
import { JwtService } from '@nestjs/jwt';
import {
  BadRequestException,
  Body,
  Controller,
  Get,
  HttpException,
  Post,
  Req,
  Res,
  UnauthorizedException,
} from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { Response } from 'express';
import { UserService } from 'src/user/user.service';
import { User } from 'src/user/user.entity';

@Controller('auth')
export class AuthController {
  constructor(
    private userService: UserService,
    private jwtService: JwtService,
    private shoppingCartService: ShoppingCartService,
  ) {}

  @Post('signup')
  async signup(@Body() user: User) {
    const { first_name, last_name, email, password } = user;

    let userFromDb = await this.userService.findOneByEmail(email);
    userFromDb = userFromDb[0];

    if (userFromDb) {
      throw new HttpException({ message: 'This email is already taken' }, 409);
    }

    const hashedPassword = await bcrypt.hash(password, 12);

    const newUser: User = {
      first_name,
      last_name,
      email,
      password: hashedPassword,
    };
    return this.userService.createUser(newUser).then((user) => {
      this.shoppingCartService.createShoppingCart({ user_email: user[0].email });
    });
  }

  @Post('login')
  async login(
    @Body('email') email: string,
    @Body('password') password: string,
    @Res({ passthrough: true }) response: Response,
  ) {
    let user = await this.userService.findOneByEmail(email);
    
    if (!user) {
      throw new BadRequestException('Email doesn`t exist');
    }

    if (!(await bcrypt.compare(password, user.password))) {
      throw new BadRequestException('Invalid user name or password');
    }

    const accessToken = await this.jwtService.signAsync({ id: user.id });

    const shoppingCart = await this.shoppingCartService.getShoppingCart(
      user.email,
    );

    if (user.is_admin) {      
      return { accessToken, shoppingCart, isAdmin: true };
    }

    return { accessToken, shoppingCart };
  }
}
