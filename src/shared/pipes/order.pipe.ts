import {
  Injectable,
  PipeTransform,
  ArgumentMetadata,
  BadRequestException,
} from '@nestjs/common';

const orderBy = ['asc', 'desc'];

@Injectable()
export class OrderPipe implements PipeTransform {
  transform(value: any, metadata: ArgumentMetadata) {
    if (value === undefined) return orderBy[0];

    if (metadata.type === 'query' && orderBy.includes(value)) {
      return value;
    }

    throw new BadRequestException('Allowed values are asc and desc');
  }
}
