import { Injectable } from '@nestjs/common';
import * as fs from 'fs';

@Injectable()
export class HelperService {
  customFileName(req, file, cb) {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1e9);
    let fileExtension = '';

    if (file.mimetype.indexOf('jpeg') > -1) {
      fileExtension = 'jpg';
    } else if (file.mimetype.indexOf('png') > -1) {
      fileExtension = 'png';
    } else if (file.mimetype.indexOf('webp') > -1) {
      fileExtension = 'webp';
    }

    const originalName = file.originalname.split('.')[0];

    cb(null, originalName + '-' + uniqueSuffix + '.' + fileExtension);
  }

  destinationPath(req, file, cb) {
    cb(null, './public/images/');
  }

  deletePicuture(pictureName) {
    const path = `${process.cwd()}/public/images/${pictureName}`;
    fs.unlink(path, (err) => {
      if (err) {
        console.log(err);
      }
    });
  }

  deleteEditedPicuture(pictureName) {
    const path = `${process.cwd()}/public/edited-images/${pictureName}`;
    fs.unlink(path, (err) => {
      if (err) {
        console.log(err);
      }
    });
  }
}
