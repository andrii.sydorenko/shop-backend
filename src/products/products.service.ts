import { Product } from './product.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { UpdateProductDTO } from './product.dto';

@Injectable()
export class ProductsService {
  columns = `id, category_id, title, price,
     description, viewed, creation_date,
     picture_path, size`;

  constructor(
    @InjectRepository(Product) private productRepo: Repository<Product>,
  ) {}

  getAllProducts(params): Promise<Product[]> {
    const { limit, offset, orderBy, sortBy } = params;

    return this.productRepo.query(
      `SELECT * FROM (
        SELECT DISTINCT ON (title) ${this.columns}
        FROM products
      ) p
      ORDER BY ${sortBy} ${orderBy}
      LIMIT ${limit}
      OFFSET ${offset}`,
    );
  }

  getFilteredProductsByPriceRangeAndOrder(params, categoryId: number) {
    const { maxPrice, minPrice, limit, offset, sortBy, orderBy } = params;
    return this.productRepo.query(
      `SELECT * from (
        SELECT DISTINCT ON(title) ${this.columns} from products
      WHERE category_id = $1 ${
        maxPrice
          ? ` AND 
      price >= ${minPrice} AND price <= ${maxPrice}`
          : ''
      }
      ) p
      ORDER BY ${sortBy} ${orderBy}
      LIMIT $2
      OFFSET $3`,
      [categoryId, limit, offset],
    );
  }

  async getProductsQuantityFilteredByPrice(
    params,
    categoryId: number,
  ): Promise<boolean> {
    const { maxPrice, minPrice } = params;
    const quantity = await this.productRepo.query(
      `SELECT COUNT(1) as "quantity" from (
        SELECT DISTINCT ON(title) id FROM products WHERE category_id = $1 ${
          maxPrice
            ? ` AND 
  price >= ${minPrice} AND price <= ${maxPrice}`
            : ''
        }
      ) q
      `,
      [categoryId],
    );

    return quantity[0].quantity;
  }

  async getSizesByTitle(title: string): Promise<{}> {
    return await this.productRepo.query(
      `SELECT id, size FROM products WHERE title = $1
      ORDER BY size asc`,
      [title],
    );
  }

  async getQuantity(): Promise<number> {
    const quantity = await this.productRepo.query(
      `SELECT count(1) as "quantity" from (
        SELECT DISTINCT ON (title) id from products
      ) q`,
    );
    return quantity[0].quantity;
  }

  getFilteredProducts(filter: string, options) {
    const { limit, offset } = options;

    return this.productRepo.query(
      `SELECT ${this.columns}
      FROM products
      WHERE LOWER(title) LIKE '%${filter.toLowerCase()}%'
      OR
      LOWER(description) LIKE '%${filter.toLowerCase()}%'
      ORDER BY category_id ASC
      LIMIT ${limit} OFFSET ${offset}`,
    );
  }

  // getAllProductsInCategory(categoryId: number): Promise<Product[]> {
  //   return this.productRepo.query(
  //     `SELECT DISTINCT ON(title) * FROM(
  //       SELECT ${this.columns}
  //       FROM products
  //       WHERE category_id = $1
  //     ) p`,
  //     [categoryId],
  //   );
  // }

  async getProductById(id: number): Promise<Product> {
    this.productRepo.query(
      'UPDATE products SET viewed = viewed + 1 WHERE id = $1',
      [id],
    );
    const product = await this.productRepo.query(
      `SELECT ${this.columns}
      FROM products
      WHERE id = $1`,
      [id],
    );
    return product[0];
  }

  async createProduct(product: Product): Promise<Product> {
    const {
      category_id,
      title,
      price,
      description,
      picture_path,
      size,
    } = product;
    const newProduct = await this.productRepo.query(
      `INSERT INTO products (category_id, title,
        price, description, picture_path, size)
        VALUES($1, $2, $3, $4, $5, $6)
        RETURNING ${this.columns}`,
      [category_id, title, price, description, picture_path, size],
    );
    return newProduct[0];
  }

  async updateProduct(id: number, product: UpdateProductDTO): Promise<Product> {
    const { price, description } = product;
    const newProduct = await this.productRepo.query(
      `UPDATE products SET price = $1, description = $2
      WHERE id = $3
      RETURNING ${this.columns}`,
      [price, description, id],
    );
    return newProduct[0][0];
  }

  deleteProduct(id: number): Promise<void> {
    return this.productRepo.query(
      `DELETE FROM products WHERE id = $1
      RETURNING picture_path`,
      [id],
    );
  }
}
