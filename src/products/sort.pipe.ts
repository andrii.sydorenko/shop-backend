import {
  Injectable,
  PipeTransform,
  ArgumentMetadata,
  BadRequestException,
} from '@nestjs/common';

const sortBy = [
  'id',
  'price',
  'title',
  'creation_date',
  'viewed',
  'category_id',
  'size',
];

@Injectable()
export class SortProductsPipe implements PipeTransform {
  transform(value: any, metadata: ArgumentMetadata) {
    if (value === undefined) return sortBy[0];

    if (metadata.type === 'query' && sortBy.includes(value)) {
      return value;
    }

    throw new BadRequestException(
      'Allowed values are id, price, title, creation_date, viewed, category_id, size',
    );
  }
}
