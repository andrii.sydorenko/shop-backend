import {
  IsInt,
  IsNumber,
  Length,
  Max,
  Min,
} from 'class-validator';
import { Column } from 'typeorm';

export class CreateProductDTO {
  @Column()
  @IsNumber()
  category_id: string;

  @Column()
  @Length(1, 255)
  title: string;

  @Column('decimal', { precision: 5, scale: 2 })
  price: number;

  @Column()
  description?: string;

  @Column()
  picture_path: string;

  @Column()
  creation_date?: number;

  @Column()
  @IsInt()
  @Min(28)
  @Max(49)
  size: number;
}

export class UpdateProductDTO {
  @Column('decimal', { precision: 5, scale: 2 })
  price: number;

  @Column()
  description?: string;
}
