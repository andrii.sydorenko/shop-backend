import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity()
export class Product {
  @PrimaryColumn()
  id?: number;

  @Column()
  category_id: string;

  @Column()
  title?: string;

  @Column()
  price: number;

  @Column()
  description?: string;

  @Column()
  picture_path: string;

  @Column()
  creation_date?: number;

  @Column()
  size: number;
}
