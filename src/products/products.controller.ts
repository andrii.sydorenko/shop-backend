import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { ProductsService } from './products.service';
import * as sharp from 'sharp';
import { HelperService } from 'src/shared/helper/helper.service';
import { Helper } from 'src/shared/helper';
import { CreateProductDTO, UpdateProductDTO } from './product.dto';
import { SortProductsPipe } from './sort.pipe';
import { OrderPipe } from 'src/shared/pipes/order.pipe';

@Controller('products')
export class ProductsController {
  constructor(
    private productService: ProductsService,
    private helperService: HelperService,
  ) {}

  @Get()
  async getAllProducts(
    @Query('filter') filter: string,
    @Query('limit', ParseIntPipe) limit: number,
    @Query('offset', ParseIntPipe) offset: number,
    @Query('sortBy', SortProductsPipe) sortBy: string,
    @Query('orderBy', OrderPipe) orderBy: string,
  ) {
    if (filter) {
      return await this.productService.getFilteredProducts(filter, {
        limit,
        offset,
      });
    }

    const params = { limit, offset, sortBy, orderBy };

    const [products, quantity] = await Promise.all([
      this.productService.getAllProducts(params),
      this.productService.getQuantity(),
    ]);

    return { products, params: { quantity } };
  }

  @Get(':id')
  async getProductById(@Param('id', ParseIntPipe) id: number) {
    const product = await this.productService.getProductById(id);
    return product;
  }

  @Get('category/:id')
  async getAllProductsInCategory(
    @Param('id', ParseIntPipe) id: number,
    @Query('maxPrice') maxPrice: number,
    @Query('minPrice') minPrice: number,
    @Query('limit', ParseIntPipe) limit: number,
    @Query('offset', ParseIntPipe) offset: number,
    @Query('sortBy', SortProductsPipe) sortBy: string,
    @Query('orderBy', OrderPipe) orderBy: string,
  ) {
    const params = {
      minPrice: Math.floor(minPrice),
      maxPrice: Math.ceil(maxPrice),
      limit,
      offset,
      sortBy,
      orderBy,
    };

    const [products, quantity] = await Promise.all([
      this.productService.getFilteredProductsByPriceRangeAndOrder(params, id),
      this.productService.getProductsQuantityFilteredByPrice(params, id),
    ]);

    return { products, params: { quantity } };
  }

  @Get('sizes/:title')
  async getSizesByTitle(@Param('title') title: string) {
    return await this.productService.getSizesByTitle(title);
  }

  @Post('picture')
  @UseInterceptors(
    FileInterceptor('picture', {
      storage: diskStorage({
        destination: Helper.destinationPath,
        filename: Helper.customFileName,
      }),
      limits: { fileSize: 2e6 },
    }),
  )
  async createProductPicture(@UploadedFile() picture) {
    await sharp(picture.path)
      .resize(200, 200)
      .toFile(`./public/edited-images/${picture.filename}`);

    return { picture_name: picture.filename };
  }

  @Post()
  async createProduct(@Body() product: CreateProductDTO) {
    return await this.productService.createProduct(product);
  }

  @Put(':id')
  async updateProduct(
    @Param('id', ParseIntPipe) id: number,
    @Body() product: UpdateProductDTO,
  ) {
    const updatedProduct = await this.productService.updateProduct(id, product);

    return updatedProduct;
  }

  @Delete(':id')
  async deleteProduct(@Param('id', ParseIntPipe) id: number) {
    const deletedProduct = await this.productService.deleteProduct(id);
    if (deletedProduct[1] === 1) {
      let pictureName = deletedProduct[0][0].picture_path;

      this.helperService.deletePicuture(pictureName);
      this.helperService.deleteEditedPicuture(pictureName);

      throw new HttpException({}, 204);
    } else {
      return { message: "product with given id doesn't exists" };
    }
  }
}
