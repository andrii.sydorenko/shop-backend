import { IsEmail } from 'class-validator';
import { Column } from 'typeorm';

export class CreateShoppingCartDTO {
  @Column()
  @IsEmail()
  user_email: string;
}
