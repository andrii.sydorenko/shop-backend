import { ShoppingCart } from './shopping-cart.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateShoppingCartDTO } from './shopping-cart.dto';

@Injectable()
export class ShoppingCartService {
  constructor(
    @InjectRepository(ShoppingCart)
    private shoppingCartRepo: Repository<ShoppingCart>,
  ) {}

  async getShoppingCart(userEmail: string): Promise<ShoppingCart> {
    const shoppingCart = await this.shoppingCartRepo.query(
      'SELECT id as "shopping_cart_id" FROM shopping_carts WHERE user_email = $1',
      [userEmail],
    );

    return shoppingCart[0];
  }

  async createShoppingCart(
    shoppingCart: CreateShoppingCartDTO,
  ): Promise<ShoppingCart> {
    const { user_email } = shoppingCart;
    const newShoppingCart = this.shoppingCartRepo.query(
      `INSERT INTO shopping_carts(user_email) VALUES($1) RETURNING id, user_email`,
      [user_email],
    );

    return newShoppingCart[0];
  }
}
