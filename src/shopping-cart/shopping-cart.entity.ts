import { Column, Entity, PrimaryColumn } from "typeorm";

@Entity()
export class ShoppingCart {
  @PrimaryColumn()
  id?: number;

  @Column()
  user_email: string;
}