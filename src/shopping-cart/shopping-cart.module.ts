import { ShoppingCartService } from './shopping-cart.service';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ShoppingCart } from './shopping-cart.entity';

@Module({
  imports: [TypeOrmModule.forFeature([ShoppingCart])],
  providers: [ShoppingCartService],
  exports: [ShoppingCartModule, ShoppingCartService],
})
export class ShoppingCartModule {}
