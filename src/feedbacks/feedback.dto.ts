import { IsInt, Length, MaxLength } from 'class-validator';
import { Column } from 'typeorm';

export class CreateFeedbackDTO {
  @Column()
  @MaxLength(70)
  user_email: string;

  @Column()
  @IsInt()
  product_id: number;

  @Column()
  @Length(1, 255)
  feedback_message: string;
}

