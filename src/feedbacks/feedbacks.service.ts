import { Feedback } from './feedbacks.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { CreateFeedbackDTO } from './feedback.dto';

@Injectable()
export class FeedbacksService {
  constructor(
    @InjectRepository(Feedback)
    private feedbackRepo: Repository<Feedback>,
  ) {}

  async getAllFeedbacksByProduct(productId: number) {
    return await this.feedbackRepo.query(
      `SELECT id, user_email, product_id,
       feedback_message, sending_date 
       FROM feedbacks WHERE product_id = $1 ORDER BY id DESC`,
      [productId],
    );
  }

  async createFeedback(feedback: CreateFeedbackDTO) {
    const { user_email, product_id, feedback_message } = feedback;
    const newFeedback = await this.feedbackRepo.query(
      `INSERT INTO feedbacks(user_email, product_id,
         feedback_message) VALUES($1, $2, $3)
      RETURNING id, user_email, product_id, feedback_message, sending_date`,
      [user_email, product_id, feedback_message],
    );
    return newFeedback[0];
  }
}
