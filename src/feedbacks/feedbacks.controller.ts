import {
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Post,
} from '@nestjs/common';
import { CreateFeedbackDTO } from './feedback.dto';
import { FeedbacksService } from './feedbacks.service';

@Controller('feedbacks')
export class FeedbacksController {
  constructor(private feedbacksService: FeedbacksService) {}

  @Get('product/:id')
  async getAllFeedbacksByProduct(@Param('id', ParseIntPipe) productId: number) {
    return await this.feedbacksService.getAllFeedbacksByProduct(productId);
  }

  @Post()
  async createFeedback(@Body() feedback: CreateFeedbackDTO) {
    return await this.feedbacksService.createFeedback(feedback);
  }
}
