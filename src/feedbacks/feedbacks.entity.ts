import { MaxLength } from "class-validator";
import { Column, Entity, PrimaryColumn } from "typeorm";

@Entity()
export class Feedback {
  @PrimaryColumn()
  id: number;

  @Column()
  @MaxLength(70)
  user_email: string;

  @Column()
  product_id: number;

  @Column()
  feedback_message: string;
}