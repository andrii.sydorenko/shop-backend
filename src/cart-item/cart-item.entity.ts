import { Entity, PrimaryColumn } from 'typeorm';

@Entity()
export class CartItem {
  @PrimaryColumn()
  id: number;
}
