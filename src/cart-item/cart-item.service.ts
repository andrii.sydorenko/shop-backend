import { CreateCartItemDTO, UpdateCartItemDTO } from './cart-item.dto';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CartItem } from './cart-item.entity';

@Injectable()
export class CartItemService {
  constructor(
    @InjectRepository(CartItem) private cartItemRepo: Repository<CartItem>,
  ) {}

  async getAllCartItemsByCartId(id: number): Promise<CartItem[]> {
    return await this.cartItemRepo.query(
      `SELECT id, product_id, quantity, shopping_cart_id
      FROM cart_items WHERE shopping_cart_id = $1`,
      [id],
    );
  }

  async getTotal(id: number) {
    const total = await this.cartItemRepo.query(
      `
    SELECT SUM(price * quantity) AS "total"
    FROM cart_items, products
    WHERE product_id = products.id AND
     shopping_cart_id = $1`,
      [id],
    );
    return total[0];
  }

  async createCartItem(
    cartItem: CreateCartItemDTO,
  ): Promise<CreateCartItemDTO> {
    const { quantity, product_id, shopping_cart_id } = cartItem;
    const newCartItem = await this.cartItemRepo.query(
      `INSERT INTO cart_items (product_id, quantity,
         shopping_cart_id) values($1, $2, $3)
      ON CONFLICT (product_id)
      DO UPDATE
      SET quantity = $2
      RETURNING id, product_id, quantity, shopping_cart_id`,
      [product_id, quantity, shopping_cart_id],
    );
    return newCartItem;
  }

  async updateCartItemByProductId(
    cartItem: UpdateCartItemDTO,
    id: number,
  ): Promise<UpdateCartItemDTO> {
    const { quantity } = cartItem;
    const newCartItem = await this.cartItemRepo.query(
      `UPDATE cart_items SET quantity = $1 WHERE product_id = $2
       RETURNING id, product_id, quantity, shopping_cart_id`,
      [quantity, id],
    );
    return newCartItem[0][0];
  }

  async deleteCartItem(id: number): Promise<boolean> {
    const data = await this.cartItemRepo.query(
      'DELETE FROM cart_items where product_id = $1',
      [id],
    );

    return !!data[1];
  }

  async clearCart(cartId: number): Promise<boolean> {
    const data = await this.cartItemRepo.query(
      'delete from cart_items where shopping_cart_id = $1',
      [cartId],
    );
    return !!data[1];
  }
}
