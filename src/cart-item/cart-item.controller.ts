import { CreateCartItemDTO, UpdateCartItemDTO } from './cart-item.dto';
import { CartItemService } from './cart-item.service';
import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  NotFoundException,
  Param,
  ParseIntPipe,
  Post,
  Put,
} from '@nestjs/common';

@Controller('cart-items')
export class CartItemController {
  constructor(private cartItemService: CartItemService) {}

  @Get('cart/:id')
  async getAllCartItemsByCartId(@Param('id', ParseIntPipe) id: number) {
    const cartItems = await this.cartItemService.getAllCartItemsByCartId(id);
    return cartItems;
  }

  @Get('total/cart/:id')
  async getTotal(@Param('id', ParseIntPipe) id: number) {
    const total = await this.cartItemService.getTotal(id);
    return total;
  }

  @Post()
  async createCartItem(@Body() cartItem: CreateCartItemDTO) {
    const newCartItem = await this.cartItemService.createCartItem(cartItem);
    return newCartItem;
  }

  @Put(':id')
  async updateCartItem(
    @Param('id', ParseIntPipe) id: number,
    @Body() cartItem: UpdateCartItemDTO,
  ) {
    const newCartItem = await this.cartItemService.updateCartItemByProductId(
      cartItem,
      id,
    );
    return newCartItem;
  }

  @Delete(':id')
  async deleteCartItem(@Param('id', ParseIntPipe) id: number) {
    const isDeleted = await this.cartItemService.deleteCartItem(id);
    if (isDeleted) {
      throw new HttpException({}, 204);
    } else {
      throw new NotFoundException();
    }
  }

  @Delete('cart/:id')
  async clearCart(@Param('id', ParseIntPipe) id: number) {
    const isCartCleared = await this.cartItemService.clearCart(id);
    if (isCartCleared) {
      throw new HttpException({}, 204);
    }
  }
}
