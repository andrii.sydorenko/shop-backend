import { IsInt } from 'class-validator';
import { Column, PrimaryColumn } from 'typeorm';

export class CreateCartItemDTO {
  @Column()
  @IsInt()
  product_id: number;

  @Column()
  @IsInt()
  quantity: number;

  @Column()
  @IsInt()
  shopping_cart_id: number;
}

export class UpdateCartItemDTO {
  @Column()
  @IsInt()
  quantity: number;
}
