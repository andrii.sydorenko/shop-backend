import { RecommendedProduct } from './recommended-products/recommendedProduct.entity';
import { Product } from './products/product.entity';
import { Category } from './categories/category.entity';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { FeedbacksController } from './feedbacks/feedbacks.controller';
import { RecommendedProductsController } from './recommended-products/recommended-products.controller';
import { AuthModule } from './auth/auth.module';
import { CategoriesService } from './categories/categories.service';
import { CategoriesController } from './categories/categories.controller';
import { ProductsController } from './products/products.controller';
import { ProductsService } from './products/products.service';
import { RecommendedProductsService } from './recommended-products/recommended-products.service';
import { ServeStaticModule } from '@nestjs/serve-static/dist/serve-static.module';
import { join } from 'path';
import { FeedbacksService } from './feedbacks/feedbacks.service';
import { Feedback } from './feedbacks/feedbacks.entity';
import { CartItemController } from './cart-item/cart-item.controller';
import { CartItemService } from './cart-item/cart-item.service';
import { CartItem } from './cart-item/cart-item.entity';
import { RedisCacheModule } from './redis-cache/redis-cache.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { SharedModule } from './shared/shared.module';
import { ShoppingCartModule } from './shopping-cart/shopping-cart.module';
import { UserModule } from './user/user.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (config: ConfigService) => ({
        type: 'postgres',
        username: config.get('DATABASE_USERNAME'),
        host: config.get('DATABASE_HOST'),
        port: +config.get('DATABASE_PORT'),
        password: config.get('DATABASE_PASSWORD'),
        database: config.get('DATABASE_NAME'),
        synchronize: false,
        autoLoadEntities: true,
        keepConnectionAlive: true,
      }),
      inject: [ConfigService],
    }),
    TypeOrmModule.forFeature([
      Category,
      Product,
      RecommendedProduct,
      CartItem,
      Feedback,
    ]),
    AuthModule,
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public/images'),
      serveRoot: '/product-images',
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public/edited-images'),
      serveRoot: '/product-images-edited',
    }),
    RedisCacheModule,
    SharedModule,
    ShoppingCartModule,
    UserModule
  ],
  controllers: [
    AppController,
    CategoriesController,
    FeedbacksController,
    RecommendedProductsController,
    ProductsController,
    CartItemController,
  ],
  providers: [
    AppService,
    CategoriesService,
    ProductsService,
    RecommendedProductsService,
    FeedbacksService,
    CartItemService,
  ],
})
export class AppModule {
  constructor() {}
}
