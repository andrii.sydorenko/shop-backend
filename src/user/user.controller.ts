import { UserService } from './user.service';
import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { User } from './user.entity';
import { CreateUserDTO } from './user.dto';
import { SortUsersPipe } from './sort.pipe';
import { OrderPipe } from 'src/shared/pipes/order.pipe';

@Controller('users')
export class UserController {
  constructor(private userService: UserService) {}

  @Get()
  async getAllUsers(
    @Query('limit', ParseIntPipe) limit: number,
    @Query('offset', ParseIntPipe) offset: number,
    @Query('sortBy', SortUsersPipe) sortBy: string,
    @Query('orderBy', OrderPipe) orderBy: string,
  ) {
    const params = { limit, offset, sortBy, orderBy };
    const users = await this.userService.getAllUsers(params);
    const quantity = await this.userService.getUsersQuantity();

    return { users, params: { quantity } };
  }

  @Get()
  async getUserById(@Param('id', ParseIntPipe) id: number) {
    return await this.userService.findOneById(id);
  }

  @Post()
  async createUser(@Body() user: CreateUserDTO) {
    return await this.userService.createUser(user).catch(() => {
      throw new HttpException(
        { message: 'User with given email already exists' },
        409,
      );
    });
  }

  @Put(':id')
  async updateUser(@Param('id', ParseIntPipe) id: number, @Body() user: User) {
    const newUser = await this.userService.updateUser(id, user);

    return newUser;
  }

  @Delete(':id')
  async deleteUser(@Param('id', ParseIntPipe) id: number) {
    const isUserDeleted = await this.userService.deleteUserById(id);

    if (isUserDeleted) {
      throw new HttpException({}, 204);
    }

    throw new HttpException(
      { message: 'user with given id does not exists' },
      404,
    );
  }
}
