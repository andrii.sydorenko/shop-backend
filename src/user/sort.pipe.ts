import {
  Injectable,
  PipeTransform,
  ArgumentMetadata,
  BadRequestException,
} from '@nestjs/common';

const sortBy = ['id', 'first_name', 'last_name', 'email', 'registration_date'];

@Injectable()
export class SortUsersPipe implements PipeTransform {
  transform(value: any, metadata: ArgumentMetadata) {
    if (value === undefined) return sortBy[0];

    if (metadata.type === 'query' && sortBy.includes(value)) {
      return value;
    }

    throw new BadRequestException(
      'Allowed values are id, first_name, last_name, email, registration_date',
    );
  }
}
