import {
  IsEmail,
  IsNotEmpty,
  Length,
  Matches,
  MaxLength,
} from 'class-validator';
import { Column } from 'typeorm';

export class CreateUserDTO {
  @Column()
  @IsNotEmpty({ message: 'First name is required' })
  @MaxLength(255)
  @Matches(/^[^!"#$%&'()*+,-./:;<=>?@[\]^`{|}~]*$/, {
    message: 'First name must not contain special characters',
  })
  first_name: string;

  @Column()
  @IsNotEmpty({ message: 'Last name is required' })
  @MaxLength(255)
  @Matches(/^[^!"#$%&'()*+,-./:;<=>?@[\]^`{|}~]*$/, {
    message: 'Last name must not contain special characters',
  })
  last_name: string;

  @Column()
  @IsNotEmpty()
  @MaxLength(70)
  @IsEmail()
  email: string;

  @Column()
  @Length(6, 255)
  password: string;

  @Column()
  is_admin?: boolean;
}
