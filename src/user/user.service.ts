import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User) private userRepository: Repository<User>,
  ) {}

  async getAllUsers(params): Promise<User[]> {
    const { limit, offset, orderBy, sortBy } = params;

    return await this.userRepository
      .query(`SELECT id, first_name as "firstName", last_name as "lastName",
      email, is_admin as "isAdmin",
      registration_date as "registrationDate" FROM users
      ORDER BY ${sortBy} ${orderBy} LIMIT ${limit} OFFSET ${offset}`);
  }

  async getUsersQuantity(): Promise<number> {
    const quantity = await this.userRepository.query(
      `SELECT COUNT(id) as "quantity" FROM users`,
    );

    return quantity[0].quantity;
  }

  async findOneByEmail(email: string): Promise<User> {
    const user = await this.userRepository.query(
      `SELECT id, first_name, last_name, email,
       is_admin,
       registration_date, password
       FROM users WHERE email = $1`,
      [email],
    );
    return user[0];
  }

  async findOneById(id: number): Promise<User> {
    const user = this.userRepository.query(
      `SELECT id, first_name, last_name, email, is_admin,
      registration_date, password
      FROM users WHERE id = $1`,
      [id],
    );
    return user[0];
  }

  async createUser(user: User): Promise<User> {
    const { first_name, last_name, email, password } = user;

    const newUser = await this.userRepository.query(
      `INSERT INTO users(first_name, last_name, email, password)
       VALUES($1, $2, $3, $4)
       RETURNING id, first_name as "firstName", last_name as "lastName",
       email, is_admin as "isAdmin", registration_date as "registrationDate"`,
      [first_name, last_name, email, password],
    );

    return newUser[0];
  }

  async updateUser(id: number, user: User): Promise<User> {
    const { is_admin } = user;
    const newUser = await this.userRepository.query(
      `UPDATE users SET is_admin = $1 WHERE id = $2
      RETURNING id, first_name, last_name,
      email, is_admin, registration_date`,
      [is_admin, id],
    );

    return newUser[0][0];
  }

  async deleteUserById(id: number): Promise<boolean> {
    const data = await this.userRepository.query(
      `DELETE FROM users WHERE id = $1`,
      [id],
    );

    return !!data[1];
  }
}
