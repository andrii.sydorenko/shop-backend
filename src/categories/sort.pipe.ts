import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  PipeTransform,
} from '@nestjs/common';

const sortBy = ['id', 'category_name'];

@Injectable()
export class SortCategoriesPipe implements PipeTransform {
  transform(value: any, metadata: ArgumentMetadata) {
    if (value === undefined) sortBy[0]

    if (metadata.type === 'query' && sortBy.includes(value)) {
      return value;
    }

    throw new BadRequestException('Allowed values are id and category_name');
  }
}
