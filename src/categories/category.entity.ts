import { Length } from "class-validator";
import { Column, Entity, PrimaryColumn } from "typeorm";

@Entity()
export class Category {
  @PrimaryColumn()
  id: number;

  @Column()
  @Length(1, 70)
  categoryName: string;
}
