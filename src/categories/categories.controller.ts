import { RedisCacheService } from 'src/redis-cache/redis-cache.service';
import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  NotFoundException,
  Param,
  ParseIntPipe,
  Put,
  Post,
  Query,
  HttpException,
} from '@nestjs/common';
import { CategoriesService } from './categories.service';
import { CreateCategoryDTO, UpdateCategoryDTO } from './category.dto';
import { SortCategoriesPipe } from './sort.pipe';
import { OrderPipe } from 'src/shared/pipes/order.pipe';

@Controller('categories')
export class CategoriesController {
  constructor(
    private categoriesService: CategoriesService,
    private redisCacheService: RedisCacheService,
  ) {}

  @Get()
  async findAll(
    @Query('limit', ParseIntPipe) limit: number = 10,
    @Query('offset', ParseIntPipe) offset: number = 0,
    @Query('sortBy', SortCategoriesPipe) sortBy: string = 'id',
    @Query('orderBy', OrderPipe) orderBy: string = 'asc',
  ) {
    const params = { limit, offset, sortBy, orderBy };
    const cacheKey = [sortBy, orderBy, offset, limit].join('_');

    const cachedCategories = await this.redisCacheService.get('shoes-cacheKey');

    if (
      cachedCategories !== null &&
      cachedCategories['shoes-cacheKey'] === cacheKey
    ) {
      return cachedCategories;
    }

    const categories = await this.categoriesService.findAll(params);
    const quantity = await this.categoriesService.getQuantity();

    const responce = {
      categories,
      params: { quantity },
    };

    this.redisCacheService.set('shoes-cacheKey', {
      ...responce,
      cacheKey,
    });

    return responce;
  }

  @Get(':id')
  async findOneById(@Param('id', ParseIntPipe) id: number) {
    const category = await this.categoriesService.findOneById(id);
    if (!category) {
      return { message: `There is no any category with id ${id}` };
    }
    return category;
  }

  @Post()
  async createCategory(@Body() category: CreateCategoryDTO) {
    const categoryDb = await this.categoriesService.findOneByCategory(
      category.category_name,
    );

    if (categoryDb) {
      return { message: 'Category already exists' };
    }
    return await this.categoriesService.createCategory(category.category_name);
  }

  @Put(':id')
  async updateOne(
    @Param('id', ParseIntPipe) id: number,
    @Body() category: UpdateCategoryDTO,
  ) {
    const newCategory = await this.categoriesService.updateCategory(
      id,
      category.category_name,
    );

    return newCategory;
  }

  @Delete(':id')
  @HttpCode(204)
  async deleteCategory(@Param('id', ParseIntPipe) id: number) {
    const isCategoryDeleted = await this.categoriesService.deleteCategory(id);

    if (isCategoryDeleted) {
      throw new HttpException({}, 204);
    } else {
      throw new NotFoundException();
    }
  }
}
