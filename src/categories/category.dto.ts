import { Length } from 'class-validator';
import { Column } from 'typeorm';

export class CreateCategoryDTO {
  @Column()
  @Length(1, 70)
  category_name: string;
}

export class UpdateCategoryDTO {
  @Column()
  @Length(1, 70)
  category_name: string;
}
