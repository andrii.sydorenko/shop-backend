import { Category } from './category.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class CategoriesService {
  constructor(
    @InjectRepository(Category)
    private categoryRepository: Repository<Category>,
  ) {}

  async findAll(options): Promise<Category[]> {
    const { limit, offset, orderBy, sortBy } = options;

    return await this.categoryRepository.query(
      `SELECT id, category_name
       FROM categories
        ORDER BY ${sortBy} ${orderBy}
        LIMIT ${limit} OFFSET ${offset}`,
    );
  }

  async getQuantity(): Promise<number> {
    const quantity = await this.categoryRepository.query(
      'SELECT count(id) as "quantity" from categories',
    );
    return quantity[0].quantity;
  }

  async findOneById(id: number): Promise<Category> {
    const category = await this.categoryRepository.query(
      'SELECT id, category_name FROM categories WHERE id = $1',
      [id],
    );
    return category[0];
  }

  async findOneByCategory(categoryName: string): Promise<Category> {
    const category = await this.categoryRepository.query(
      'SELECT * FROM categories WHERE category_name = $1',
      [categoryName],
    );
    return category[0];
  }

  async createCategory(category: string): Promise<Category> {
    const newCategory = await this.categoryRepository.query(
      'INSERT INTO categories(category_name) VALUES($1) RETURNING id, category_name',
      [category],
    );

    return newCategory[0];
  }

  async updateCategory(id: number, categoryName: string): Promise<Category> {
    const category = await this.categoryRepository.query(
      'UPDATE categories SET category_name = $1 WHERE id = $2 RETURNING id, category_name',
      [categoryName, id],
    );
    return category[0][0];
  }

  async deleteCategory(id: number): Promise<boolean> {
    const data = await this.categoryRepository.query(
      'DELETE FROM categories WHERE id = $1',
      [id],
    );

    return !!data[1];
  }
}
