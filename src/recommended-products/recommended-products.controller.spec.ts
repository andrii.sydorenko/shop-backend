import { Test, TestingModule } from '@nestjs/testing';
import { RecommendedProductsController } from './recommended-products.controller';

describe('RecommendedProductsController', () => {
  let controller: RecommendedProductsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RecommendedProductsController],
    }).compile();

    controller = module.get<RecommendedProductsController>(RecommendedProductsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
