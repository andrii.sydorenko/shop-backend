import { IsInt } from 'class-validator';
import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity()
 export class RecommendedProduct {
   @PrimaryColumn()
   id?: number;

   @Column()
   @IsInt()
   productId: number;
 }