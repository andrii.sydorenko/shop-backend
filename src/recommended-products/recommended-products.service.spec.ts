import { Test, TestingModule } from '@nestjs/testing';
import { RecommendedProductsService } from './recommended-products.service';

describe('RecommendedProductsService', () => {
  let service: RecommendedProductsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RecommendedProductsService],
    }).compile();

    service = module.get<RecommendedProductsService>(RecommendedProductsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
