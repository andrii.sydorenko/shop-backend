import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  Param,
  ParseIntPipe,
  Post,
} from '@nestjs/common';
import { RecommendedProductsService } from './recommended-products.service';

@Controller('recommended-products')
export class RecommendedProductsController {
  constructor(private recommendedProductsService: RecommendedProductsService) {}
  @Get()
  async getAllRecommendedProducts() {
    const recommendedProducts = await this.recommendedProductsService.getAllRecommendedProducts();
    return recommendedProducts;
  }

  @Post()
  async addRecommendedProductById(@Body('productId', ParseIntPipe) id: number) {
    const isRecommended = await this.recommendedProductsService.isRecommended(
      id,
    );

    if (isRecommended) {
      const recommendedProduct = await this.recommendedProductsService.createRecommendedProduct(
        id,
      );
      return recommendedProduct;
    }
    throw new HttpException({ message: "product is already recommended" }, 409)
  }

  @Delete(':id')
  async removeRecommendedProductById(@Param('id', ParseIntPipe) id: number) {
    const recommendedProduct = await this.recommendedProductsService.deleteRecommendedProduct(
      id,
    );

    if (recommendedProduct[1] !== 0) {
      throw new HttpException({}, 204);
    } else {
      return { message: "product with given id doesn't exists" };
    }
  }
}
