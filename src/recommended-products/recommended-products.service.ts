import { RecommendedProduct } from './recommendedProduct.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class RecommendedProductsService {
  constructor(
    @InjectRepository(RecommendedProduct)
    private recommendedProduct: Repository<RecommendedProduct>,
  ) {}

  async getAllRecommendedProducts(): Promise<RecommendedProduct[]> {
    return await this.recommendedProduct.query(
      `SELECT DISTINCT ON(title) * FROM(
          SELECT products.id, category_id, title, price,description,
          viewed,creation_date, picture_path, size, recommended_products.id
            FROM products
          INNER JOIN recommended_products ON products.id = product_id
        ) p`,
    );
  }

  async createRecommendedProduct(id: number): Promise<RecommendedProduct> {
    const product = await this.recommendedProduct.query(
      `INSERT INTO recommended_products(product_id) VALUES($1)
      RETURNING id`,
      [id],
    );
    return product[0];
  }

  deleteRecommendedProduct(id: number): Promise<boolean> {
    return this.recommendedProduct.query(
      'DELETE FROM recommended_products WHERE product_id = $1',
      [id],
    );
  }

  isRecommended(productId: number): Promise<boolean> {
    return this.recommendedProduct.query(
      'SELECT id FROM recommended_products WHERE product_id = $1',
      [productId],
    );
  }
}
